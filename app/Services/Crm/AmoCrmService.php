<?php

namespace App\Services;

use App\Models\AmoCrmToken;
use App\Services\Crm\CrmInterface;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Log;

class AmoCrmService implements CrmInterface
{
    public const LINK_PREFIX = 'https://';
    public const LINK_POSTFIX = '.amocrm.ru/api/v2/';
    public const CURLOPT_USERAGENT = 'amoCRM-API-client/1.0';

    public const URI_CONTACTS = 'contacts';
    public const URI_TASKS = 'tasks';

    public const TASK_TYPE_CALL = 1;
    public const TASK_TYPE_MEET = 2;
    public const TASK_TYPE_MAIL = 3;

    public const ELEMENT_TYPE_USER = 1;

    /**
     * @param string $name
     */
    public function userRequestNotificationHandle(string $name)
    {
        if (!empty(env('CRM_AMO_SUBDOMAIN'))
            && !empty(env('CRM_AMO_CLIENT_ID'))
            && !empty(env('CRM_AMO_CLIENT_SECRET'))
        ) {
            $token = $this->getToken();
            if ($token) {
                $contactResponse = $this->addContact($token, $name);
                $crmUserId = $contactResponse['_embedded']['items'][0]['id'] ?? null;
                if ($crmUserId) {
                    $this->addTask($token, $crmUserId, self::ELEMENT_TYPE_USER, self::TASK_TYPE_MAIL);
                } else {
                    Log::error('Cant get CrmUserId for ' . $name, $contactResponse);
                }
            }
        }
    }

    /**
     * @param string $token
     * @param string $name
     * @return mixed
     */
    public function addContact(string $token, string $name)
    {
        $contacts['add'] = [
            [
                'name' => $name,
                'created_at' => now()->toString(),
            ]
        ];
        return $this->sendRequest($token, self::URI_CONTACTS, $contacts, true);
    }

    /**
     * @param string $token
     * @param int $elementId
     * @param int $elementType
     * @param int $taskType
     * @param string|null $text
     * @return mixed
     */
    public function addTask(string $token, int $elementId, int $elementType, int $taskType, string $text = NULL)
    {
        $tasks['add'] = [
            [
                'element_id' => $elementId,
                'element_type' => $elementType,
                'task_type' => $taskType,
                'text' => $text,
            ]
        ];
        return $this->sendRequest($token, self::URI_TASKS, $tasks, true);
    }

    /**
     * @return mixed
     */
    public function getToken()
    {
        $token = $this->findFreshAccessToken();
        if ($token) {
            return $token;
        } else {
            $tokenData = $this->getNewTokenByRefreshToken();
            if (empty($tokenData)) {
                $tokenData = $this->getFirstTimeTokens();
            }
            $token = $tokenData['access_token'];
            $this->saveTokens($tokenData);
        }
        return $token;
    }

    /**
     * @return array
     */
    private function getFirstTimeTokens()
    {
        $data = [
            'client_id' => env('CRM_AMO_CLIENT_ID'),
            'client_secret' => env('CRM_AMO_CLIENT_SECRET'),
            'grant_type' => env('CRM_AMO_GRANT_TYPE'),
            'code' => env('CRM_AMO_CODE'),
            'redirect_uri' => env('CRM_AMO_REDIRECT_URI'),
        ];
        return $this->oAuthRequest($data);
    }

    /**
     * @return array|null
     */
    private function getNewTokenByRefreshToken()
    {
        $refresToken = $this->getLastRefreshToken();
        if ($refresToken) {
            $data = [
                'client_id' => env('CRM_AMO_CLIENT_ID'),
                'client_secret' => env('CRM_AMO_CLIENT_SECRET'),
                'grant_type' => 'refresh_token',
                'refresh_token' => $refresToken,
                'redirect_uri' => env('CRM_AMO_REDIRECT_URI'),
            ];
        } else {
            return null;
        }
        return $this->oAuthRequest($data);
    }

    /**
     * @param array $data
     * @return array
     */
    private function oAuthRequest(array $data)
    {
        $curl = curl_init(); //Сохраняем дескриптор сеанса cURL
        /** Устанавливаем необходимые опции для сеанса cURL  */
        curl_setopt($curl,CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl,CURLOPT_USERAGENT,'amoCRM-oAuth-client/1.0');
        curl_setopt($curl,CURLOPT_URL, 'https://' . env('CRM_AMO_SUBDOMAIN') . '.amocrm.ru/oauth2/access_token');
        curl_setopt($curl,CURLOPT_HTTPHEADER,['Content-Type:application/json']);
        curl_setopt($curl,CURLOPT_HEADER, false);
        curl_setopt($curl,CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($curl,CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($curl,CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($curl,CURLOPT_SSL_VERIFYHOST, 2);
        $out = curl_exec($curl); //Инициируем запрос к API и сохраняем ответ в переменную
        $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);
        if (!in_array((int)$code, [200,204])) {
            Log::error('AmoCrm api error ' . $code, [$data]);
        }
        return json_decode($out, true);
    }

    /**
     * @param array $tokensData
     */
    private function saveTokens(array $tokensData)
    {
        if (!empty($tokensData['access_token'])) {
            AmoCrmToken::create([
               'type' =>  AmoCrmToken::TYPE_ACCESS,
                'value' => $tokensData['access_token'],
            ]);
        }
        if (!empty($tokensData['refresh_token'])) {
            AmoCrmToken::create([
                'type' =>  AmoCrmToken::TYPE_REFRESH,
                'value' => $tokensData['refresh_token'],
            ]);
        }
    }

    /**
     * @param string $access_token
     * @param string $uri
     * @param array $data
     * @param bool $post
     * @return mixed
     */
    private function sendRequest(string $access_token, string $uri, array $data, bool $post = false)
    {
        $link = self::LINK_PREFIX . env('CRM_AMO_SUBDOMAIN') . self::LINK_POSTFIX . $uri;
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_USERAGENT, 'amoCRM-API-client/1.0');
        curl_setopt($curl, CURLOPT_URL, $link);
        if($post) {
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
            curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
            curl_setopt($curl, CURLOPT_HTTPHEADER, [
                'Content-Type: application/json',
                'Authorization: Bearer ' . $access_token,
            ]);
        }
        curl_setopt($curl, CURLOPT_HEADER, false);
//        curl_setopt($curl, CURLOPT_COOKIEFILE, dirname(__FILE__) . '/cookie.txt');
//        curl_setopt($curl, CURLOPT_COOKIEJAR, dirname(__FILE__) . '/cookie.txt');
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2);
        $out = curl_exec($curl);
        $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);

        if (!in_array((int)$code, [200,204])) {
            Log::error('AmoCrm api error ' . $code, [$link, $data]);
        }

        return json_decode($out, true);
    }

    /**
     * @return Builder|Model|object|null
     */
    private function findFreshAccessToken()
    {
        return AmoCrmToken::where('type', AmoCrmToken::TYPE_ACCESS)->where('created_at', '>=', Carbon::today()->startOfDay()->toDateTimeString())
            ->where('created_at', '<', Carbon::tomorrow()->startOfDay()->toDateTimeString())->latest()->first()->value;
    }

    /**
     * @return Builder|Model|object|null
     */
    private function getLastRefreshToken()
    {
        return AmoCrmToken::where('type', AmoCrmToken::TYPE_REFRESH)->latest()->first()->value;
    }

}