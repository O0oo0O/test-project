<?php

namespace App\Services;

use App\Exceptions\UserRequestResolutionException;
use App\Models\UserRequest;
use App\Repositories\UserRequestRepository;
use Illuminate\Support\Facades\Auth;
use Exception;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class UserRequestService
{
    public const DEFAULT_LIST_LIMIT = 10;

    /**
     * @var UserRequestRepository
     */
    protected $userRequestRepository;

    /**
     * UserRequestService constructor.
     * @param UserRequestRepository $userRequestRepository
     */
    public function __construct(UserRequestRepository $userRequestRepository)
    {
        $this->userRequestRepository = $userRequestRepository;
    }

    /**
     * @param int $userId
     * @param array $data
     * @return UserRequest|Model
     * @throws UserRequestResolutionException
     */
    public function createRequest(int $userId, array $data)
    {
        $this->checkRequestResolutionForUser($userId);
        return $this->userRequestRepository->createRequest(array_merge($data, ['user_id' => $userId]));
    }

    /**
     * @param int $userId
     * @return bool
     */
    public function checkTodaySendingRequestByUserId(int $userId): bool
    {
        return $this->userRequestRepository->checkTodaySendingRequestByUserId($userId);
    }

    /**
     * @param int $userId
     * @throws UserRequestResolutionException
     */
    private function checkRequestResolutionForUser(int $userId)
    {
        if ($this->checkTodaySendingRequestByUserId($userId)) {
            throw new UserRequestResolutionException();
        }
    }

    /**
     * @param int $limit
     * @return LengthAwarePaginator|null
     */
    public function getRequestsList($limit = self::DEFAULT_LIST_LIMIT)
    {
        if ($this->checkAccess()) {
            return $this->userRequestRepository->getList($limit);
        } else {
            return null;
        }
    }

    /**
     * @return bool
     */
    private function checkAccess(): bool
    {
        return Auth::user()->isManager();
    }

    /**
     * @param int $id
     * @return UserRequest|UserRequest[]|Collection|Model|null
     * @throws Exception
     */
    public function markAsAnsweredById(int $id)
    {
        return $this->checkAccess() ? $this->userRequestRepository->markAsReadById($id) : null;
    }
}