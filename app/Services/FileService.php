<?php

namespace App\Services;

use Illuminate\Http\UploadedFile;

class FileService
{
    public const USER_FILE_PREFIX = 'users';

    /**
     * @param UploadedFile $file
     * @return string|null
     */
    public function upload(UploadedFile $file)
    {
        $localPath = $file->store(self::USER_FILE_PREFIX);
        return $this->getAbsoluteUrl($localPath);
    }

    /**
     * @param string|NULL $path
     * @return string|null
     */
    private function getAbsoluteUrl(string $path = null): ?string
    {
        return  $this->getStoragePrefix() . '/' . $path;
    }

    /**
     * @return string|null
     */
    private function getStoragePrefix(): ?string
    {
        return config('filesystems.disks.public.url');
    }
}