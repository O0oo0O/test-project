<?php

namespace App\Http\Middleware;

use Closure;
use Debugbar;

class DebugBarMiddleware
{
    const COOKIE_VALUE_FOR_DEBUG_BAR = 'i_known_what_i_am_doing';

    /**
     * @param $request
     * @param Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(config('app.laravel-debugbar')) {
            Debugbar::enable();
        } else {
            Debugbar::disable();
        }

        return $next($request);
    }
}
