<?php

namespace App\Providers;

use App\Services\AmoCrmService;
use App\Services\Crm\CrmInterface;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(CrmInterface::class, AmoCrmService::class);
    }
}
