<?php


namespace App\Repositories;


use App\Models\UserRequest;
use Carbon\Carbon;
use Exception;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class UserRequestRepository extends AbstractRepository
{
    public const ITEM_CLASS_NAME = UserRequest::class;

    /**
     * @param int $userId
     * @return bool
     */
    public function checkTodaySendingRequestByUserId(int $userId)
    {
        return UserRequest::where('user_id', $userId)
            ->where('created_at', '>=', Carbon::today()->startOfDay()->toDateTimeString())
            ->where('created_at', '<', Carbon::tomorrow()->startOfDay()->toDateTimeString())->first() ? true : false;
    }

    /**
     * @param array $data
     * @return UserRequest|Model
     */
    public function createRequest(array $data)
    {
        return UserRequest::create($data);
    }

    /**
     * @param int $limit
     * @return LengthAwarePaginator
     */
    public function getList(int $limit)
    {
        return UserRequest::with('user')->orderBy('id','desc')->paginate($limit);
    }

    /**
     * @param int $id
     * @return UserRequest|UserRequest[]|Collection|Model|null
     * @throws Exception
     */
    public function markAsReadById(int $id)
    {
        $userRequest = UserRequest::find($id);
        if ($userRequest) {
            $userRequest->is_answered = true;
            $userRequest->save();
        } else {
            throw new Exception("User request $id not found");
        }

        return $userRequest;
    }
}