<?php

namespace App\Repositories;

use Exception;
use Illuminate\Database\Eloquent\Model;

abstract class AbstractRepository
{
    const ITEM_CLASS_NAME = '';

    /**
     * @return string
     * @throws Exception
     */
    protected function getItemClassName() : string
    {
        if (!static::ITEM_CLASS_NAME) {
            throw new Exception("Model class name isn't set");
        }

        return static::ITEM_CLASS_NAME;
    }

    /**
     * @param array $data
     * @throws Exception
     */
    public function store(array $data)
    {
        $className = $this->getItemClassName();

        try {
            /** @var Model $className */
            $className::create($data);
        } catch (Exception $exception) {
            \Log::error($exception->getMessage(), $data);
            throw $exception;
        }
    }
}