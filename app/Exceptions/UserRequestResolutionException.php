<?php


namespace App\Exceptions;

use Exception;

class UserRequestResolutionException extends Exception
{
    public const MESSAGE_KEY = 'layout.send-request-resolution-error';

    /**
     * UserRequestResolutionException constructor.
     * @param string $message
     * @param int $code
     * @param \Throwable|NULL $previous
     */
    public function __construct(string $message = "", int $code = 0, \Throwable $previous = null)
    {
        parent::__construct($message ?: __(static::MESSAGE_KEY), $code, $previous);
    }
}