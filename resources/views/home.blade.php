@extends('layouts.app')

@section('content')
<div class="container">
    @error('error')
        <div class="form-group row">
            <div class="col">
                <span class="invalid-feedback d-block" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            </div>
        </div>
    @enderror
    <div class="row justify-content-center">
        @if ($list !== null)
            @include('partials.users-requests-list')
        @else
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('layout.contact-form') }}</div>
                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        @if($sendRequestPermission)
                            @include('partials.contact-form')
                        @else
                            {{ __('layout.today-already-send') }}
                        @endif
                    </div>
                </div>
            </div>
        @endif
    </div>
</div>
@endsection
