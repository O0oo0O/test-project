@extends('layouts.app')

@section('content')
    <div class="flex-center position-ref full-height">
        <div class="top-right links">
            @auth
                <a href="{{ route('client.home') }}">Home</a>
            @endauth
        </div>

        <div class="content">
            <div class="title m-b-md">
                Welcome
            </div>
        </div>
    </div>
@endsection
