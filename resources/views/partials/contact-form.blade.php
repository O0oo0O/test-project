@error('upload')
    <div class="form-group row">
        <div class="col">
            <span class="invalid-feedback d-block" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        </div>
    </div>
@enderror
<form method="POST" action="{{ route('client.send.request') }}" enctype="multipart/form-data">
    @csrf
    <div class="form-group row">
        <label for="subject" class="col-md-4 col-form-label text-md-right">{{ __('layout.subject') }}</label>
        <div class="col-md-6">
            <input id="subject" type="text" class="form-control @error('subject') is-invalid @enderror" name="subject" value="{{ old('subject') }}" required autocomplete="subject" autofocus maxlength="100">
            @error('subject')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row">
        <label for="message" class="col-md-4 col-form-label text-md-right">{{ __('layout.message') }}</label>
        <div class="col-md-6">
            <textarea id="message" type="text" class="form-control @error('message') is-invalid @enderror" name="message" required maxlength="1000"> {{ old('message') }}</textarea>
            @error('message')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row">
        <label for="file" class="col-md-4 col-form-label text-md-right">{{ __('layout.add-file') }}</label>
        <div class="col-md-6">
            <input id="file" type="file" class="form-control border-0 @error('file') is-invalid @enderror" name="file">
            @error('file')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>

    <div class="form-group row mb-0">
        <div class="col-md-8 offset-md-4">
            <button type="submit" class="btn btn-primary">
                {{ __('layout.send') }}
            </button>
        </div>
    </div>
</form>