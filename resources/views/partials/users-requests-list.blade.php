@if(!empty($list))
    <div class="col list-wrapper">
        <div class="card">
            <div class="card-header">
                {{ __('layout.contact-form') }}
            </div>
            <div class="card-body">
                <hr>
                @foreach($list as $item)
                    <?php /** @var \App\Models\UserRequest $item */ ?>
                    <div class="form-group row">
                        <div class="col-1">
                            {{ $item->id }}
                        </div>
                        <div class="col-1">
                            {{ $item->user->name }}
                        </div>
                        <div class="col">
                            {{ $item->user->email }}
                        </div>
                        <div class="col">
                            {{ $item->subject }}
                        </div>
                        <div class="col message-container">
                            {{ $item->message }}
                        </div>
                        <div class="col-1">
                            <a href="{{ $item->file_path }}" target="_blank">
                                file
                            </a>
                        </div>
                        @if($item->is_answered)
                            <div class="col green">
                                {{ __('layout.answered') }}
                            </div>
                        @else
                            <div class="col">
                                <form method="POST" action="{{ route('client.mark.as.read', ['id' => $item->id]) }}">
                                    @csrf
                                    <button type="submit" class="btn btn-danger">
                                        {{ __('layout.mark-as-answered') }}
                                    </button>
                                </form>
                            </div>
                        @endif
                    </div>
                    <hr>
                @endforeach
                {{ $list->links() }}
            </div>
        </div>
    </div>
@endif