<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('welcome');

Auth::routes();

Route::group(['middleware' => ['auth'], 'as' => 'client.', 'prefix' => '/client'], function () {
    Route::get('/home', 'HomeController@index')->name('home');
    Route::post('/send-request', 'RequestController@sendRequest')->name('send.request');
    Route::post('/mark-as-read/{id}', 'RequestController@markAsReadById')->name('mark.as.read');
});

