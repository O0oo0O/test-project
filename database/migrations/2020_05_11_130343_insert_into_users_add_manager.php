<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class InsertIntoUsersAddManager extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $password = Hash::make('password');
        \DB::connection()->getPdo()->exec(<<<SQL
            INSERT INTO users (name, password, type, email) 
                VALUES ('First Manager', '{$password}', 'manager', 'test@domain.com')
SQL
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \DB::connection()->getPdo()->exec(<<<SQL
            DELETE FROM users
                WHERE type = 'manager' AND name = 'First Manager'; 
SQL
        );
    }
}
