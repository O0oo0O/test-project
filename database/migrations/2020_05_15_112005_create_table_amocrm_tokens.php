<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableAmocrmTokens extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \DB::connection()->getPdo()->exec(<<<'SQL'
            CREATE TABLE ammocrm_tokens (
                id SERIAL NOT NULL  PRIMARY KEY,
                type TEXT NOT NULL ,
                value TEXT NOT NULL,                
                created_at TIMESTAMP(0) WITH TIME ZONE NOT NULL DEFAULT now(),
                updated_at TIMESTAMP(0) WITH TIME ZONE,                
                deleted_at TIMESTAMP(0) WITH TIME ZONE
            );

            CREATE INDEX ammocrm_tokens_created_at_index
                ON ammocrm_tokens (created_at);
SQL
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ammocrm_tokens');
    }
}
