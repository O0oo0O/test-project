<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableUserRequests extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \DB::connection()->getPdo()->exec(<<<'SQL'
            CREATE TABLE users_requests (
                id SERIAL NOT NULL  PRIMARY KEY,
                user_id INT NOT NULL REFERENCES users (id) ON DELETE CASCADE,
                subject TEXT DEFAULT NULL,
                message TEXT DEFAULT NULL,
                file_path TEXT DEFAULT NULL,
                is_answered BOOLEAN DEFAULT FALSE,
                created_at TIMESTAMP(0) WITH TIME ZONE NOT NULL DEFAULT now(),
                updated_at TIMESTAMP(0) WITH TIME ZONE,
                deleted_at TIMESTAMP(0) WITH TIME ZONE
            );

            CREATE INDEX users_requests_user_id_index
                ON users_requests (user_id);
SQL
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_requests');
    }
}
